CC=gcc
CFLAGS=-std=c90 -m32 -fno-stack-protector -no-pie -fno-pic -Wl,-rpath,.

.PHONY: default
default: loopy-0

%: %.c
	$(CC) -o $@ $< $(CFLAGS)
